@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ __('Dashboard') }}</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif


            <table id="posts-table" class="table table-bordered display nowrap" cellspacing="0" style="width: 100%">
                <thead>
                    <tr>
                        <th data-priority="1">Id</th>
                        <th>Full name</th>
                        <th>Email</th>
                        <th>Birth date</th>
                        <th>Post title</th>
                        <th class="none">Description</th> {{-- post description always collapsed --}}
                        <th data-priority="2">Post datetime</th>
                    </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

        </div>

    </div>
</div>

<script type="text/javascript">
    $(function () {
        let table = $('#posts-table').DataTable({
            ajax: '{{ route('posts.index') }}',
            serverSide: true, //data processed server-side
            pageLength: 25, //default authors per page are 25
            processing: true,
            lengthChange: false,
            responsive: { //make the child row responsive
                details: {
                    //override the default "white-space: nowrap" of datatable nowrap class to "white-space: normal" in each child row
                    renderer: function ( api, rowIdx, columns ) {
                        let data = $.map( columns, function ( col, i ) {
                            let row = '';
                            if(col.hidden){
                                row = '<div class="row">'
                                row += '<div class="col-md-2" style="white-space: normal;">'+col.title+': </div>'
                                row += '<div class="col-md-10" style="white-space: normal;">'+col.data+'</div>'
                                row += '</div>'
                                return row;
                            }
                            return row;
                        } ).join('');

                        return data ? $('<table/>').append( data ) : false;
                    }
                }
            },
            columns: [
                {data: 'id', name: 'id', searchable: false},
                {data: 'full_name', name: 'full_name'},
                {data: 'email', name: 'email', searchable: false, render:
                    function(data){ return '<a href="mailto:'+data+'">'+data+'</a>'; } //email field as a mailto link
                },
                {data: 'birthdate', name: 'birthdate', searchable: false, render:
                    function(data){ return moment(data).format('MMMM DD, YYYY'); } //June 30, 2020
                },
                {data: 'title', name: 'title', orderable: false},
                {data: 'description', name: 'description', orderable: false, searchable: false},
                {data: 'date', name: 'date', searchable: false, render: function(data){
                    return data ? moment(data).format('MMMM DD, YYYY h:mma') : 'N/A'; } //June 30, 2020 01:23pm | N/A
                },

            ]
        });
    });
</script>

@endsection
