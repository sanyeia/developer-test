<?php

use App\Author;
use App\Post;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Test User',
            'email' => 'test.mail@example.com',
            'password' => Hash::make('secret'),
        ]);

        factory(Author::class, 100)->create()->each(function ($author) {
            $author->posts()->saveMany(factory(Post::class, 1)->make());
        });
    }
}