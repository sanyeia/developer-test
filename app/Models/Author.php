<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public $timestamps = false;

    protected $dates = [
        'added',
    ];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
